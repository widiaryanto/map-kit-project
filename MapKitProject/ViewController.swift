//
//  ViewController.swift
//  MapKitProject
//
//  Created by Muhamad Widi Aryanto on 05/09/19.
//  Copyright © 2019 Widi Aryanto. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mapView.mapType = .standard
        let location = CLLocationCoordinate2D(latitude: -7.099073, longitude: 110.281914)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Muhamad Widi Aryanto"
        annotation.subtitle = "This is the home of a mobile developers (Android & iOS)"
        mapView.addAnnotation(annotation)
    }


}

